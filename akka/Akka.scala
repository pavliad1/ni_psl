package Akka

import akka.actor.{Actor, ActorSystem, Props}
import language.postfixOps

case class Sort(list: List[Int])

case class SortedHalf(list: List[Int])

case class SortHalf(list: List[Int])

abstract class MergeSortActor extends Actor {
  def merge(left: List[Int], right: List[Int]): List[Int] = {
    (left, right) match {
      case (left, Nil) => left
      case (Nil, right) => right
      case (leftHead :: leftTail, rightHead :: rightTail) => {
        if (leftHead < rightHead) leftHead :: merge(leftTail, right)
        else rightHead :: merge(left, rightTail)
      }
    }
  }
}

class MainSorter extends MergeSortActor {
  def receive = {
    case sort: Sort => {
      val child = context.actorOf(Props[Sorter])
      child ! SortHalf(sort.list)
    }
    case half: SortedHalf => {
      println(s"Sorted: ${half.list}")
      context.system.terminate()
    }
  }

}

class Sorter extends MergeSortActor {
  var sortedOtherHalf = None: Option[List[Int]]

  def receive = {
    case whole: SortHalf => {
      val n = whole.list.length / 2
      if (n == 0) {
        sender ! SortedHalf(whole.list)
      }
      else {
        val (left, right) = whole.list.splitAt(n)
        val lActor = context.actorOf(Props[Sorter])
        val rActor = context.actorOf(Props[Sorter])
        lActor ! SortHalf(left)
        rActor ! SortHalf(right)
      }
    }
    case half: SortedHalf => {
      (sortedOtherHalf) match {
        case (None) => sortedOtherHalf = Some(half.list)
        case (Some(h: List[Int])) => context.parent ! SortedHalf(merge(sortedOtherHalf.get, half.list))
      }
    }
  }
}

object Main {

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("MergeSort")
    val numbers = List(5, 2, 4, 9, 1, 2, 3, 7, 5, -1)
    val sorter = system.actorOf(Props[MainSorter], "MainSorter")
    println(s"Sorting: ${numbers}")
    sorter ! Sort(numbers)
  }
}