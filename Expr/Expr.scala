import scala.util.parsing.combinator.JavaTokenParsers

object Expr extends JavaTokenParsers {

  lazy val exp: Parser[Double] = term ~ rep("+" ~ term | "-" ~ term) ^^ {
    res => {
      var acc = res._1
      res._2.foreach((s) => {
        s._1 match {
          case "+" => acc += s._2
          case "-" => acc -= s._2
        }})
      acc
    }
  }

  lazy val term: Parser[Double] = factor ~ rep("*" ~ factor | "/" ~ factor) ^^ {
    res => {
      var acc = res._1
      res._2.foreach((s) => {
        s._1 match {
          case "/" => acc /= s._2
          case "*" => acc *= s._2
        }})
      acc
    }
  }

  lazy val factor: Parser[Double] = floatingPointNumber ^^ (_.toDouble) | factor2 | factor3

  lazy val factor2: Parser[Double] = "(" ~ exp ~ ")" ^^ {
    res => res._1._2
  }

  lazy val factor3: Parser[Double] = "-"~factor ^^ {
    res => -res._2
  }

  def main(args: Array[String]): Unit = {

    val inputs : List[String] = List("--(1+-3)*4", "1*2/3*4/5*6", "(6*5)+((1/5)*10)", "(6*(5+5)-(11*10)+1000)", "((((5/5))))")
    inputs.foreach( s => println(s"$s = ${parseAll(exp, s)}") )
  }
}
