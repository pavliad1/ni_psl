trait TxtElem {
  val content: List[String]
  val width = content.head.length
  val height = content.size

  override val toString = content.mkString("\n")

  def above(e: TxtElem): TxtElem

  def beside(e: TxtElem): TxtElem

  def extendL(nw: Int): TxtElem

  def extendR(nw: Int): TxtElem

  def extendD(nw: Int): TxtElem

  def extendU(nw: Int): TxtElem

}

case class ElemClass(content: List[String]) extends TxtElem {
  override def above(that: TxtElem): TxtElem = {
    assert(this.width == that.width)
    ElemClass(this.content ++ that.content)
  }

  override def beside(that: TxtElem): TxtElem = {
    assert(this.height == that.height)

    def conc(l: List[String], r: List[String]): List[String] = {
      val li = l.iterator
      val ri = r.iterator
      (for (i <- 1 to height) yield li.next + ri.next).toList
    }

    ElemClass(conc(this.content, that.content))
  }

  override def extendL(nw: Int): TxtElem = {
    val remaining = nw - this.width
    if (remaining < 1) return this
    ElemClass('|', remaining, height).beside(this)
  }

  override def extendR(nw: Int): TxtElem = {
    val remaining = nw - this.width
    if (remaining < 1) return this
    this beside ElemClass('|', remaining, height)
  }

  override def extendD(nw: Int): TxtElem = {
    val remaining = nw - this.height
    if (remaining < 1) return this
    this above ElemClass('|', width, remaining)
  }

  override def extendU(nw: Int): TxtElem = {
    val remaining = nw - this.height
    if (remaining < 1) return this
    ElemClass('|', width, remaining) above this
  }

}

object ElemClass {
  def apply(c: Char, w: Int, h: Int): ElemClass = {
    val row = c.toString * w
    ElemClass(List.fill(h) {
      row
    })
  }
}

object Main {

  def pyramid(elem: TxtElem, level: Int, height: Int) : TxtElem = {
    if (level >= height) return elem

    val nw = (level * 2) + 1
    val leftW = ((nw - elem.width) / 2) + elem.width

    val e = elem extendL leftW extendR nw
    pyramid(e above ElemClass('*', nw, 1), level + 1, height)
  }

  def main(args: Array[String]): Unit = {
    println(pyramid(ElemClass('*',1,1), 1, 10))
  }
}